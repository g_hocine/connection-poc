package com.cvous.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.cvous.models.Participation;

public interface ParticipationRepository extends PagingAndSortingRepository<Participation, Integer> {

}
